const express = require("express");
const app = express();
var cors = require('cors')

const mongoose = require("mongoose");
const cookieParser = require("cookie-parser");
const dotenv = require("dotenv");
dotenv.config();
const bodyParser = require("body-parser");
const logger = require("morgan");
const { API_KEY } = require("./config.js");
// const port = process.env.PORT || 4040;
app.use(logger("dev"));
app.use("/uploads", express.static("uploads"));


/*========= Here we want to let the server know that we should expect and allow a header with the content-type of 'Authorization' ============*/
app.use(cors());
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Headers", "Content-type,Authorization");
  next();
});
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());

// mongoose models    .
const { Rider, UserModel } = require("./models/models.js");

mongoose.set("useCreateIndex", true);
mongoose.set("useFindAndModify", false);


app.use('/api', require('./routes'));


//MONGOOSE CONNECTION
mongoose
  .connect(process.env.API_KEY2, { useNewUrlParser: true })
  .then(() => {
    app.listen(4000, () => {
      console.log("Listening on port 4000");
    });
  })
  .catch(error => {
    console.log({
      message: `Unable to establish a connection to the server ${error}`
    });
  });
