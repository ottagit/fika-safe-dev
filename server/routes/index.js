// create new Router instance for api routes
var Router = require("express").Router();

// mount our 'riders and saccos' router onto the API router
Router.use("/riders", require("./rider"));

Router.use("/saccos", require("./sacco"));

Router.use("/admin", require("./admin"));

Router.use("/sms", require("./sms"));

module.exports = Router;
