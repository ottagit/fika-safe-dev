let Router = require("express").Router();
const { Sacco } = require("../../models/models.js");
const jwt = require("jsonwebtoken");
const exjwt = require("express-jwt");
const bcrypt = require("bcrypt");

const jwtMW = exjwt({
  secret: "keyboard cat 4 ever"
});

Router.get(`/`,(req, res) => {
    const { status, dateLte, dateGte } = req.query; // destructuring
    console.log(new Date(dateLte));
    console.log(new Date(dateGte));
    if (status) {
      Sacco.find()
        .where("status")
        .equals(status)
        .sort({ created: -1 })
        .exec()
        .then(saccos => {
          res.status(200).json(saccos);
          console.log(saccos);
        })
        .catch(err => {
          res.send(`Internal server error${err.stack}`).status(400);
        });
    } else if (dateGte && dateLte) {
      Sacco.find()
        .where("created")
        .gt(new Date(dateGte))
        .lt(new Date(dateLte))
        .sort({ created: -1 })
        .exec()
        .then(saccos => {
          res.status(200).json(saccos);
          console.log(saccos);
        })
        .catch(err => {
          res.send(`Internal server error${err.stack}`).status(400);
        });
    } else {
      Sacco.find()
        .sort({ created: -1 })
        .exec()
        .then(saccos => {
          res.status(200).json(saccos);
          console.log(saccos);
        })
        .catch(err => {
          res.send(`Internal server error${err.stack}`).status(400);
        });
    }
  })
  Router.post(`/`,(req, res) => {
    const { email, password } = req.body;

    console.log(req.body);
    const newSacco = new Sacco(req.body);
    // if (!new_sacco._id) new_sacco._id = Schema.Types.ObjectId;
    newSacco
      .save()
      .then(sacco => {
        console.log({ message: "The sacco was added successfully" });

        // creating a nodemailer test account
        //       nodemailer.createTestAccount((err, account) => {
        //         const htmlEmail = `
        // <h3>Login Details</h3>
        // <ul>
        // <li>email:${email}</li>
        // <li>password:${password}</li>
        // </ul>
        // `;
        //         // the accoutn that will be sending the mails
        //         let transporter = nodemailer.createTransport({
        //           host: 'stmp.gmail.com',
        //           port:465,
        //           secure:false,
        //           service: 'gmail',
        //           auth: {
        //             user: '#######@gmail.com',
        //             pass: '#######',
        //           },
        //         });

        //         // mail options
        //         let mailOptions = {
        //           from: `#######@gmail.com`,
        //           to: email,
        //           subject: 'Fika-Safe',
        //           html: htmlEmail,
        //         };

        //         // initiating the nodemailer sending options
        //         transporter.sendMail(mailOptions, (err, info) => {
        //           if (err) throw err;
        //           console.log(info);
        //         });
        //       });

        res.status(200).json({ sacco });
      })
      .catch(err => {
        res.status(400).send({ message: `Unable to add the sacco: ${err}` });
      });
  });

Router.get(`/:id`,(req, res) => {
    // parameter
    let saccoId;
    try {
      saccoId = req.params.id;
      console.log(saccoId);
    } catch (error) {
      res.json({ message: `Invalid sacco id ${error}` });
    }

    Sacco.findById({ _id: saccoId })
      .then(sacco => {
        res.json(sacco).status(200); // this a single object rbeing returned
      })
      .catch(err => {
        res.send(`Internal server error${err}`).status(400);
      });
  })

  Router.put(`/:id`,(req, res) => {
    let saccosId;
    console.log(req.params.id);
    try {
      saccosId = req.params.id;
    } catch (error) {
      res.status(400).send({ message: `Invalid saccos ID:${saccosId}` });
    }
    const newSacco = req.body;

    Sacco.findByIdAndUpdate({ _id: saccosId }, newSacco, {
      returnNewDocument: true,
      new: true,
      strict: false
    })
      .find({ _id: saccosId })
      .then(updatedSacco => {
        res.json(updatedSacco);
      })
      .catch(err => {
        console.log(err);
        res
          .status(500)
          .json({ message: `Unable to update the saccos information ${err}` });
      });
  });

Router.delete(`/:id`, jwtMW,(req, res) => {
  let saccosId;
  try {
    saccosId = req.params.id;
    console.log(saccosId);
  } catch (error) {
    res.status(400).send({ message: `Invalid saccos ID:${saccosId}` });
  }
  // THE REQ.BODY IS OPTIONAL INTHE FINDBYIDANREMOVE METHOD
  Sacco.findByIdAndRemove({ _id: saccosId })
    .then(result => {
      res.json(result);
    })
    .catch(err => {
      console.log({ message: `Unable to delelete the saccos profile ${err}` });
    });
});

Router.post(`/login`,(req, res) => {
  const { email, password } = req.body;
  console.log("User submitted: ", email, password);

  Sacco.findOne({ email: email }).then(user => {
    console.log("User Found: ", user);
    if (user === null) {
      res.json(false);
    }
    bcrypt.compare(password, user.password, function(err, result) {
      if (result === true) {
        console.log("Valid!");
        let token = jwt.sign({ email: user.email }, "keyboard cat 4 ever", {
          expiresIn: 129600
        }); // Signing the token
        res.json({
          sucess: true,
          err: null,
          token
        });
      } else {
        console.log("Entered Password and Hash do not match!");
        res.status(401).json({
          sucess: false,
          token: null,
          err: "Entered Password and Hash do not match!"
        });
      }
    });
  });
});

module.exports = Router;
